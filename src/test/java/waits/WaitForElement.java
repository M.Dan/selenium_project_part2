package waits;

import driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitForElement {

    private static final int WAIT_TIME = 10;

    private static WebDriverWait getWebDriverWait(){
        return new WebDriverWait(DriverManager.getWebDriver(),WAIT_TIME);
    }

    private static WebDriverWait getWebDriverWait(int time){
        return new WebDriverWait(DriverManager.getWebDriver(),time);
    }

    public static void waitForElementVisible(WebElement webElement){
        WebDriverWait wait = getWebDriverWait();
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void waitForElementVisible(WebElement webElement, int time){
        WebDriverWait wait = getWebDriverWait(time);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void waitForElementClickable(WebElement webElement){
        WebDriverWait wait = getWebDriverWait();
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static void waitForElementSelected(WebElement webElement){
        WebDriverWait wait = getWebDriverWait();
        wait.until(ExpectedConditions.elementToBeSelected(webElement));

    }

    public static void waitForElementInvisibleLocated(WebElement webElement){
        WebDriverWait wait = getWebDriverWait();
        wait.until(ExpectedConditions.invisibilityOf(webElement));
    }

    public static void waitFrameToBeAvailableAndSwitchToIt(String frameLocator)
    {
        WebDriverWait wait = getWebDriverWait();
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));

    }





}
