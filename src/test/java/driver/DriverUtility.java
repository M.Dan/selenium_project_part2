package driver;

import org.openqa.selenium.WebDriver;


public class DriverUtility {
    private static WebDriver driver = DriverManager.getWebDriver();


    public static void setUrl(String Url){
        driver.navigate().to(Url);

    }

    public static void initialDriverConfiguration(){

        driver.manage().window().maximize();
    }
}
