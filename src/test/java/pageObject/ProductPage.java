package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage extends BasePage {

    //quantity of product
    @FindBy(css = "input[name='quantity']")
    private WebElement quantity_of_product;

    //button add to cart
    @FindBy(css = "button[name='add-to-cart']")
    private WebElement button_add_to_cart;

    //button add to wishlist
    @FindBy(css = "*[class='yith-wcwl-add-button']>a")
    private WebElement add_to_wishlist;

    //show wishlist_if_product_add
    @FindBy(css = "*[class='yith-wcwl-wishlistaddedbrowse']>a")
    private WebElement show_wishlist_product_add;

    //show wishlist_if_product_exist
    @FindBy(css = "*[class='yith-wcwl-wishlistexistsbrowse']>a")
    private WebElement show_wishlist_product_exist;

    ////category tag
    //@FindBy(css="*[class='posted_in']>a")
    //private WebElement category_tag;

    //label_product_right_site
    @FindBy(className = "storefront-product-pagination__title")
    private WebElement mobile_pagination_label;

    //tab description
    @FindBy(id = "tab-title-description")
    private WebElement tab_description;

    //tab additional info
    @FindBy(id = "tab-title-additional_information")
    private WebElement tab_additional_info;

    //tab reviews
    @FindBy(id = "tab-title-reviews")
    private WebElement tab_rewiews;

    //add to cart scroll
    @FindBy(css = "*[class='storefront-sticky-add-to-cart__content-button button alt']")
    private WebElement button_scroll_add_to_cart;

    public ProductPage() {
        super();
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public ProductPage addToCartProduct() {
        button_add_to_cart.click();
        return this;
    }

    public ProductPage addToWishlistProduct() {
        add_to_wishlist.click();
        return this;
    }

    public ListOfWishesPage goToWishlistIfNoProduct() {
        show_wishlist_product_add.click();
        return new ListOfWishesPage();
    }

    public ListOfWishesPage goToWishlistIfExistedProduct() {
        show_wishlist_product_exist.click();
        return new ListOfWishesPage();
    }

    // public ClimbingCategoryPage goToCategory(){
    //  category_tag.click();
    //   return new ClimbingCategoryPage();


    public ProductPage addToCartWhenScroll() {
        button_scroll_add_to_cart.click();
        return this;
    }

    public ProductPage selectDesription() {
        tab_description.click();
        return this;
    }

    public ProductPage selectAdditionalInfo() {
        tab_additional_info.click();
        return this;
    }

    public ProductPage selectReviews() {
        tab_rewiews.click();
        return this;
    }


}




