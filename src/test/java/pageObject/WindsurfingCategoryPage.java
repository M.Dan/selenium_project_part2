package pageObject;


import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;


public class WindsurfingCategoryPage extends BasePage{

    @FindBy(css="*[class = 'woocommerce-products-header__title page-title']")
    private WebElement windsurfingHeader;

    //Sort field
    @FindBy(className="orderby")
    private WebElement orderby_field;


    //Price_filtr
    @FindBy(className="price_slider_wrapper")
    private WebElement filtr_price;

    //Filtr submit
    @FindBy(xpath="//button[(text()='Filtruj')]")
    private WebElement filtr_submit;

    //Select first product_windsurfing
    @FindBy(xpath = "//li[contains(@class,'post-386')]/a[1]")
    private WebElement select_product_386;

    //Add to cart_first_product
    @FindBy(xpath = "//li[contains(@class,'post-386')]/a[2]")
    private WebElement add_to_cart_386;

    //go to Cart Egipt
    @FindBy(xpath = "//li[contains(@class,'post-386')]/a[contains(@class,'added_to_cart')]")
    private WebElement go_to_cart_386;

    //Select product 393
    @FindBy(xpath = "//li[contains(@class,'post-393')]/a[1]")
    private WebElement select_product_393;

    //Add to cart_product_393
    @FindBy(xpath = "//li[contains(@class,'post-393')]/a[2]")
    private WebElement add_to_cart_393;

    //Select product 391
    @FindBy(xpath = "//li[contains(@class,'post-391')]/a[1]")
    private WebElement select_product_391;

    //Add to cart_product_391
    @FindBy(xpath = "//li[contains(@class,'post-391')]/a[2]")
    private WebElement add_to_cart_391;

    //Select product 50
    @FindBy(xpath = "//li[contains(@class,'post-50')]/a[1]")
    private WebElement select_product_50;

    //Add to cart_product_50
    @FindBy(xpath = "//li[contains(@class,'post-50')]/a[2]")
    private WebElement add_to_cart_50;

    //Select product 389
    @FindBy(xpath = "//li[contains(@class,'post-389')]/a[1]")
    private WebElement select_product_389;

    //Add to cart_product_389
    @FindBy(xpath = "//li[contains(@class,'post-389')]/a[2]")
    private WebElement add_to_cart_389;

    //go to Cart Green Islands
    @FindBy(xpath = "//li[contains(@class,'post-389')]/a[contains(@class,'added_to_cart')]")
    private WebElement go_to_cart_389;


    //See your cart
    @FindBy(css="*[class='added_to_cart wc-forward']")
    private WebElement see_user_cart;



    public WindsurfingCategoryPage(){
        super();
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public boolean isWindCategoryDisplayCorrectly(){
        WaitForElement.waitForElementVisible(windsurfingHeader,6);
        return getWindsurfingHeader().contains("Windsurfing");
    }

    public WindsurfingCategoryPage selectSortOption(){
        orderby_field.click();
        return this;
    }

    public WindsurfingCategoryPage filtrSubmit(){
        filtr_submit.click();
        return this;
    }



    public CartPage goToCart() {
        WaitForElement.waitForElementVisible(see_user_cart,6);
        see_user_cart.click();
        return new CartPage();
    }

    public ProductPage selectProduct50(){
        select_product_50.click();
        return new ProductPage();
    }

    public WindsurfingCategoryPage addProduct50ToCart(){
        WaitForElement.waitForElementClickable(add_to_cart_50);
        add_to_cart_50.click();
        WaitForElement.waitForElementVisible(see_user_cart);
        return this;
    }

    public ProductPage selectProduct389(){
        select_product_389.click();
        return new ProductPage();
    }

    public WindsurfingCategoryPage addProduct389ToCart(){
        add_to_cart_389.click();
        WaitForElement.waitForElementVisible(go_to_cart_389);
        return this;
    }

    public ProductPage selectProduct386(){
        select_product_386.click();
        return new ProductPage();
    }

    public WindsurfingCategoryPage addProduct386ToCart(){
        add_to_cart_386.click();
        WaitForElement.waitForElementVisible(go_to_cart_386);
        return this;
    }

    public ProductPage selectProduct391(){
        select_product_391.click();
        return new ProductPage();
    }

    public WindsurfingCategoryPage addProduct391ToCart(){
       //WaitForElement.waitForElementVisible(add_to_cart_391,6);
        add_to_cart_391.click();
        return this;
    }

    public ProductPage selectProduct393(){
        select_product_393.click();
        return new ProductPage();
    }

    public WindsurfingCategoryPage addProduct393ToCart(){
        add_to_cart_393.click();
        return this;
    }

    public String getWindsurfingHeader(){
        return windsurfingHeader.getText();
    }





}
