package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class ListOfWishesPage extends BasePage {

    //remove product
    @FindBy(css="*[class='remove remove_from_wishlist']")
    private WebElement remove_product_from_wishlist;

    @FindBy(className = "entry-title")
    private WebElement wishlistHeader;

    //add to cart
    @FindBy(css="a[rel='nofollow']")
    private WebElement add_to_cart_from_wishlist;


    //info_about_successfully added to cart
    @FindBy(className = "woocommerce-message")
    private WebElement info_about_succes_added;

    //info about remove product
    @FindBy(className="woocommerce-message")
    private WebElement label_about_remove_product_from_wishlist;

    //change title_in_wishlist
    @FindBy(css="*[class='wishlist-title wishlist-title-with-form']>h2")
    private WebElement change_title_in_wishlist;

    //edit title wishlist
    @FindBy(css="a[class='btn button show-title-form']")
    private WebElement edit_title_in_wishlist;

    //enter title of wishlist
    @FindBy(name="wishlist_name")
    private WebElement enter_wishlist_name;

    //save title of wishlist
    @FindBy(name="save_title")
    private WebElement save_title;

    //cancel title of wishlist
    @FindBy(css="a[class='hide-title-form btn button']")
    private WebElement cancel_title;



    public ListOfWishesPage(){
        super();
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public boolean isWishlistDisplayCorrectly(){
        WaitForElement.waitForElementVisible(wishlistHeader,6);
        return getHeader().equals("Lista życzeń");
    }

    public ListOfWishesPage removeProduct(){
        remove_product_from_wishlist.click();
        return this;
    }

    public ListOfWishesPage getInfoAboutRemove(){
        label_about_remove_product_from_wishlist.getText();
        return this;

    }

    public CartPage addProductsToCart(){
        add_to_cart_from_wishlist.click();
        return new CartPage();

    }

    public ListOfWishesPage chanceTitleOfWishList(String title){
        change_title_in_wishlist.click();
        enter_wishlist_name.clear();
        enter_wishlist_name.sendKeys(title);
        save_title.click();
        return this;

    }
    public ListOfWishesPage chanceTitleOfWishListPerEdit(String title){
        edit_title_in_wishlist.click();
        enter_wishlist_name.clear();
        enter_wishlist_name.sendKeys(title);
        save_title.click();
        return this;
    }

    public ListOfWishesPage cancelEditTitle(){
        cancel_title.click();
        return this;
    }

    public String getHeader(){
        return wishlistHeader.getText();
    }




}
