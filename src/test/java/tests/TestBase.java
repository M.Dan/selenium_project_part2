package tests;

import driver.DriverManager;
import driver.DriverUtility;
import org.testng.annotations.*;
import pageObject.BasePage;

public class TestBase {

    @BeforeClass
    public void beforeTest() {
        DriverManager.getWebDriver();
        DriverUtility.initialDriverConfiguration();
        DriverUtility.setUrl("https://fakestore.testelka.pl/");

    }

    @AfterClass
    public void afterTest() {
        DriverManager.disposeDriver();
    }
}
