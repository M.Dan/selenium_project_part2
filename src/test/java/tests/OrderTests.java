package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObject.*;

public class OrderTests extends TestBase {

    @Test
    public void realiseOrder(){
        BasePage basePage = new BasePage();
        basePage.closeCookieInfo();
        HomePage homePage = basePage.selectHomePage();
        Assert.assertTrue(homePage.isHomepageIsCorrectly());
        WindsurfingCategoryPage windsurfingCategoryPage = homePage.goToWindsurfing();
        Assert.assertTrue(windsurfingCategoryPage.isWindCategoryDisplayCorrectly());
        windsurfingCategoryPage.addProduct50ToCart();
        Assert.assertEquals(windsurfingCategoryPage.getAmountOfProductsInCart(), "1 Produkt", "Enter diffrent than 1 product in cart" );
        windsurfingCategoryPage.addProduct386ToCart();
       Assert.assertEquals(windsurfingCategoryPage.getAmountOfProductsInCart(), "2 Produkty", "Enter diffrent than 2 products in cart" );
        windsurfingCategoryPage.addProduct389ToCart();
      Assert.assertEquals(windsurfingCategoryPage.getAmountOfProductsInCart(), "3 Produkty", "Enter diffrent than 3 products in cart" );
        CartPage cartPage = windsurfingCategoryPage.goToCart();
        Assert.assertTrue(cartPage.isCartpageDisplayCorrectly());
        Assert.assertEquals(cartPage.cartSize(), 3, "You haven't 3 products in cart");
        OrderPage orderPage = cartPage.goToCash();
        Assert.assertTrue(orderPage.isOrderDisplayCorrectly());
        orderPage.insertDataCash().createAccount().insertCardDate().buyAndPayOrder();


    }
}
