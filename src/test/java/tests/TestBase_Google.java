package tests;

import driver.DriverManager;
import driver.DriverUtility;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class TestBase_Google {

    @BeforeClass
    public void beforeTest() {
        DriverManager.getWebDriver();
        DriverUtility.initialDriverConfiguration();
        DriverUtility.setUrl("https://www.google.com/");
    }

    @AfterClass
    public void afterTest() {
        DriverManager.disposeDriver();
    }
}
